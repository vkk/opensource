import sys, subprocess
from PyQt5.QtWidgets import * 

class GoodGoodDisplay(QWidget):

   def __init__(self, parent=None):
      super(GoodGoodDisplay, self).__init__(parent)

      self.initThatGoodGood()

   def initThatGoodGood(self):

      urlLabel = QLabel("url")
      self.urlLine = QLineEdit()

      authorLabel = QLabel("by")
      self.authorLine = QLineEdit()

      titleLabel = QLabel("title")
      self.titleLine = QLineEdit()

      self.runButton = QPushButton("Run")
      self.cancelButton = QPushButton("Cancel")

      urlLayout = QHBoxLayout()
      urlLayout.addWidget(urlLabel)
      urlLayout.addWidget(self.urlLine)

      infoLayout = QHBoxLayout()
      infoLayout.addWidget(authorLabel)
      infoLayout.addWidget(self.authorLine)
      infoLayout.addWidget(titleLabel)
      infoLayout.addWidget(self.titleLine)

      buttonLayout = QHBoxLayout()
      buttonLayout.addWidget(self.runButton)
      buttonLayout.addWidget(self.cancelButton)

      combineLayout = QVBoxLayout()
      combineLayout.addLayout(urlLayout)
      combineLayout.addLayout(infoLayout)
      combineLayout.addLayout(buttonLayout)

      windowLayout = QGridLayout()
      windowLayout.addLayout(combineLayout, 0, 0)

      self.runButton.clicked.connect(self.extractAudio)
      self.cancelButton.clicked.connect(self.closeDisplay)
      self.setLayout(windowLayout)
      self.setWindowTitle("get that good good")

   def extractAudio(self):
      url = self.urlLine.text()

      if url == "":
         QMessageBox.information(self, "Empty Entry",
            "Paste YouTube URL")
         subprocess.run(["youtube-dl", "--hls-prefer-ffmpeg", "--extract-audio",
            "--audio-format 'mp3'", "--audio-quality 0", "--prefer-ffmpeg"])
         return
      else:
         QMessageBox.information(self, "Got it!",
            "Downloading audio from %s" % url)
         

   def closeDisplay(self):
      sys.exit()

if __name__ == '__main__':

   app = QApplication(sys.argv)

   display = GoodGoodDisplay() 
   display.show()

   sys.exit(app.exec_())
